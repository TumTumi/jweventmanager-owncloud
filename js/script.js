/**
 * ownCloud - jweventmanager
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Tobias Matthaiou <develop@tobimat.eu>
 * @copyright Tobias Matthaiou 2015
 */

var tmjzNavigation = function() {
	this.tmjzFormular = '';
};

tmjzNavigation.init = function () {
	this.tmjzFormular = $('#tmjzFormulare');
};

tmjzNavigation.showForm = function (name) {
	var element = $('#' + name),
		content = element.html();
	this.tmjzFormular.html(content);
};

tmjzNavigation.clickElement = function (link) {
	var formname = $(link).data('template');
	this.showForm(formname);
};


(function ($, OC, nav) {
	$(document).ready(function () {
		nav.init();
		nav.showForm('formVortrag');

		$(".newCalendar").click(function() {
			nav.clickElement(this);
		})

	});
})(jQuery, OC, tmjzNavigation);