<h1>Termine erstellen:</h1>

<form action="newEvent" method="POST" enctype="multipart/form-data">
	<div id="tmjzFormulare"></div>
	<button>Senden</button>
</form>

<template id="formVortrag">
	<h2>Vortrag</h2>
	<?php print_unescaped($this->inc('form.vortrag')); ?>
</template>

<template id="formMeineAufgabe">
	<h2>Meine Aufgagen</h2>
	<?php print_unescaped($this->inc('form.meine_aufgabe')); ?>
</template>